import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Tech Shop';
  products: any[] = [];

  constructor(public http: HttpClient) {

  }

  ngOnInit() {
    this.http.get('https://dummyjson.com/products?limit=5').subscribe(
      (data:any) => {
        this.products = data?.products
        console.log(this.products)
      }
    )
  }
}


